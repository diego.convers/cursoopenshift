FROM maven AS inicio
RUN mkdir /opt/project
WORKDIR /opt/project
RUN git clone http://www.firefly-e.com.co/dconvers1979/cursodocker.git
WORKDIR ./cursodocker
RUN mvn package

FROM payara/server-full
WORKDIR /opt/payara/deployments
COPY --from=inicio /opt/project/cursodocker/target/cursodocker-1.0-SNAPSHOT.war .
